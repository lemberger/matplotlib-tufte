# This file is part of matplotlib-tufte, Tufte-style plots for matplotlib.
# https://gitlab.com/lemberger/matplotlib-tufte
#
#
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: 2022 Thomas Lemberger <https://thomaslemberger.com>

import sys
import matplotlib.pyplot as plt
import hypothesis
import hypothesis.strategies as strategy
import tuftelike


def random_xs_and_ys(x_unique=False):
    if x_unique:
        return strategy.lists(
            strategy.tuples(strategy.integers(), strategy.integers()),
            unique_by=lambda t: t[0],
            min_size=2,
        )
    return strategy.lists(
        strategy.tuples(strategy.integers(), strategy.integers()),
        min_size=2,
    )


@hypothesis.given(random_xs_and_ys(x_unique=True))
@hypothesis.settings(max_examples=1000)
def test_adjust_plot_from_gca_succeeds(xs_and_ys):
    xs = [x for x, _ in xs_and_ys]
    ys = [y for y, _ in xs_and_ys]

    plt.plot(xs, ys)
    tuftelike.adjust(xs, ys)


def test_adjust_plot_from_gca_large():
    xs = [9170110573141995, 9170110573141999]
    ys = xs

    plt.plot(xs, ys)
    tuftelike.adjust(xs, ys)


def test_adjust_plot_from_gca_small():
    xs = [-917011057314199522, -917011057314199523]
    ys = xs

    plt.plot(xs, ys)
    tuftelike.adjust(xs, ys)


def test_adjust_plot_from_gca_bounds():
    xs = [-sys.maxsize, sys.maxsize]
    ys = xs

    plt.plot(xs, ys)
    tuftelike.adjust(xs, ys)


@hypothesis.given(random_xs_and_ys(x_unique=True))
@hypothesis.settings(max_examples=10)
def test_adjust_plot_given_succeeds(xs_and_ys):
    xs = [x for x, _ in xs_and_ys]
    ys = [y for y, _ in xs_and_ys]

    axes = plt.subplot()
    axes.plot(xs, ys)
    tuftelike.adjust(xs, ys, axes)


@hypothesis.given(random_xs_and_ys())
def test_adjust_scatter_from_gca(xs_and_ys):
    xs = [x for x, _ in xs_and_ys]
    ys = [y for y, _ in xs_and_ys]

    plt.scatter(xs, ys)
    tuftelike.adjust(xs, ys)


@hypothesis.given(random_xs_and_ys(x_unique=True))
def test_adjust_bar_from_gca(xs_and_ys):
    xs = [x for x, _ in xs_and_ys]
    ys = [y for y, _ in xs_and_ys]

    plt.bar(xs, ys)
    tuftelike.adjust(xs, ys)
